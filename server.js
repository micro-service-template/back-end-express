const express = require('express')
const app = express()
var ReactDOMServer = require('react-dom/server');
var test = require('../frontend/src/index');
app.get('/home', (req, res) => {
  res.json({"title":"hello world!"})
})

app.get('/test', (req, res) => {
  const body = renderToString(test);

  res.send(body);
});

app.listen(5000, () => {
  console.log('Start server at port 3000.')
})